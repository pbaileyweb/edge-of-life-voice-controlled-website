            if (annyang) {
                
                
            // Let's define a command.
            var commands = {
                'back': function() {window.history.back()},
                'forward': function() {window.history.go(1)},
                'home': function() { window.location.assign("/index.html") },
                'animals': function() { window.location.assign("/animals.html") },
                'climate': function() { window.location.assign("/climate.html") },
                'resources': function() { window.location.assign("/resources.html") },
                
                'Efforts in your area': function() { window.location.assign("https://www.rspb.org.uk/our-work/conservation/") },
                'The JNCC': function() { window.location.assign("http://jncc.defra.gov.uk/") },
                'Make a pledge': function() { window.location.assign("https://support.wwf.org.uk/donate-to-wwf?pc=ASF001001&ds_medium=cpc&ds_rl=1263317&ds_rl=1263317") },
                
                'menu': function() { $("#mobilenavigationwindow").toggleClass('hidden-class')},
                
                
                'orangutan': function() { window.location.assign("/animals-orangutan.html") },
                'sumatran elephant': function() { window.location.assign("/animals-elephant.html") },
                'jaguar': function() { window.location.assign("/animals-jaguar.html") },
                'tiger': function() { window.location.assign("/animals-tiger.html") },
                'sea turtle': function() { window.location.assign("/animals-seaturtle.html") },
                'galapagos penguin': function() { window.location.assign("/animals-penguin.html") },
                
                'go to the top': function() {$('html, body').animate({ scrollTop: 0 }, 0);},
                'go to the bottom': function() {$('html, body').animate({ scrollTop: 9999 }, 0);},
                'scroll up': function() {$('html, body').animate({ scrollTop: $("html").scrollTop() - 460 }, 0);},
                'scroll down': function() {$('html, body').animate({ scrollTop: $("html").scrollTop() + 460 }, 0);},
            }

            // Add our commands to annyang
            annyang.addCommands(commands);

            // Start listening.`
            annyang.start();
            }
